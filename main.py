from pymongo import MongoClient
import pandas as pd
import datetime
from logger_config.logger import get_logger
from db.tracker_store_connection import mongo_get_all_collection_data
from utils.date import get_previous_month, get_month_name, get_first_and_last_date_of_month, get_last_15_dates
#URL = "mongodb://root:root@172.16.22.5:27019/?authSource=admin&readPreference=primary&directConnection=true&ssl=false"
# host = "zrechjd.gnani.site"
# DB_NAME = "indo_star_db"
# port = 8080
# COLLECNTION = "voice_blast_report"
# authMechanism = "SCRAM-SHA-256"
# authSource = "indo_star_db"
# URL = "http://"+str(host)+":"+str(port)
logger = get_logger(__name__) 
import collections

def main_function():
    phone_number = 7041295305
    condition= {"phone_number":phone_number}
    # list_of_last_15_days_date
    # condition = {"phone_number": phone_number, "input_date":{'$in':list_of_last_15_days_date} }
    records =mongo_get_all_collection_data(col_type="input",q1=condition)
    input_date_list_from_db = [i['input_date'] for i in records]
    # collect = collections.Counter(input_date_list_from_db)
    for r in records:

        print(f"\n\n------------------------ {r['input_date']} - {r['duplicate']} -----------------")
        input_date = r['input_date']
        l = get_last_15_dates(input_date)
        print("last 15 dates : ",l)
        count = 0
        print("[",end=" ")
        for i in input_date_list_from_db:
            if i in l:
                print(i,end="\t")
                count+=1
        print("]")
        # count+=collect[input_date]
        print(f"count : {count} <====> duplicate : {r['duplicate']}")
       
        if count >= r['duplicate']:
            print(f"result is : True for {r['input_date']}")
        else:
            print(f"result is : False for {r['input_date']}")
            break

            # print(records)
    # get_last_15_dates()

    

if __name__ == "__main__":
    print("======Start======\n")
    main_function()
    print("======end======")
