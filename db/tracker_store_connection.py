#!/usr/bin/env python3
# -- coding: utf-8 --

# Author: Aakansha
# Date: 02-03-2022
import sys
sys.path.append('../')

import os
import yaml
from utils.text_format import color
from logger_config.logger import get_logger
import json
import requests
# from raven import Client
from datetime import datetime
from utils.date import get_previous_month, get_month_name

# name of the module for logging 
logger = get_logger(__name__) 

## colors and text format
red = color.RED
blue = color.BLUE
cyan = color.CYAN
darkcyan = color.DARKCYAN
green = color.GREEN
purple= color.PURPLE
yellow = color.YELLOW
bold = color.BOLD
underline = color.UNDERLINE
end = color.END

def load_config():
    conf = ""
    try:
        # environment = os.environ["BOT_ENV"]
#        environment = "dev"
        global environment
        environment = "dev"
        # environment = "prod"
        logger.info("Bot Environment is :"+str(environment))
        if environment == "prod":
            endpoint_filename = "endpoints.yml"                
        else:
            endpoint_filename = "endpoints_dev.yml" 
        with open('configs/'+endpoint_filename, 'r') as f:
            conf = yaml.load(f, Loader=yaml.FullLoader) 
    
    except Exception as e:
        logger.exception("Error loading the configuration from the endpoints.yml "+str(e))
    return conf

#-------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------
endpoints = load_config()
# client = Client(gen_config["raven"]["url"])

headers = {
        'app': 'mongo-db-layer',
        'Content-Type': 'application/json'
    }

def mongo_url_fn():
    mongo_host = endpoints.get("mongo_api").get("url")
    mongo_port = endpoints.get("mongo_api").get("port")
    if environment == "prod":
        logger.info("url is " + "http://" + str(mongo_host) + ":" + str(mongo_port))
        return "https://" + str(mongo_host)
    else:
       return "http://" + str(mongo_host) + ":" + str(mongo_port)

def mongo_get_all_collection_data(col_type="input",q1={}):
    
    '''this function will return the document from the collection'''
    
    db = endpoints.get("mongo_api").get("db_name", "99_acres")
    input_collection = endpoints.get("mongo_api").get("input_collection", "customer_details")
    
    
    if col_type == "input":
        COL = input_collection
    else:
        exit()
    mongo_url = mongo_url_fn()
    
    mongo_url = mongo_url+"/"+"find"
    logger.info(f"mongo url : {mongo_url}")
    # logger.debug("URL:"+str(mongo_url))
    # logger.info("URL:"+str(mongo_url))
    logger.info(f"\ncol : {COL}\n DB: {db}")
    payload={
                "db":db,
                "collection" : COL,
                "dict_query": q1
            }
    headers = {
        'app': 'mongo-db-layer',
        'Content-Type': 'application/json'
    }
    try:
        
        response = requests.request("POST", mongo_url, headers=headers, json=payload)
        logger.info(response.text)
        res = response.json()
        print(f"Response : {res}")
        #logger.debug("RES :"+str(response.text))
        #logger.info("RES :"+str(res))
        if res['message']=="Record Does not Exist" and res['status']==200:
            logger.info('Record Does not Exist')
            return ""
        elif res['status']==404:
            logger.info('Error from the server')
            return False
        else:
            #logger.debug('Record Fetched Successfully')
            reterived_collection = res['result'] 
            # li = ["last_triggered_date","callConnectedTime","callEndTime","next_trigger_date"]
            # for k,v in reterived_collection.items():
            #     if k in li:
            #         logger.debug(f"time is ----------------> {v}")
            #         reterived_collection[k] = datetime.strptime(v[0:19], '%Y-%m-%d %H:%M:%S')            
            # logger.info(f"\n\n\n{reterived_collection}")
            return reterived_collection
    except Exception as e:
        logger.exception("Exception in mongo api url --> "+ str(e))
        return []


#-----------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------- 
