from datetime import timedelta
import datetime


month_mapping = {
    "01": "Jan",
    "02": "Feb",
    "03": "Mar",
    "04": "Apr",
    "05": "May",
    "06": "Jun",
    "07": "Jul",
    "08": "Aug",
    "09": "Sep",
    "10": "Oct",
    "11": "Nov",
    "12": "Dec"
}

month_mapping_reverse = {
    "Jan": "01",
    "Feb": "02",
    "Mar": "03",
    "Apr": "04",
    "May": "05",
    "Jun": "06",
    "Jul": "07",
    "Aug": "08",
    "Sep": "09",
    "Oct": "10",
    "Nov": "11",
    "Dec": "12"
}


def get_previous_month():
    today = datetime.date.today()
    first = today.replace(day=1)
    last_month = first - datetime.timedelta(days=1)
    month = last_month.strftime("%m")
    year = last_month.strftime("%Y")
    return year, month


def get_month_name(month):
    return month_mapping[month]


def get_first_and_last_date_of_month(month_name):
    import calendar
    import datetime
    import pytz
    month = int((month_mapping_reverse[month_name]).lstrip("0"))
    first_day, day_count = calendar.monthrange(2023,month)

    yesterday = datetime.datetime.now(pytz.timezone('Asia/Kolkata')) - datetime.timedelta(days=1) # on 1st of every month it will pick last month's details
    first_day = yesterday.replace(month= month, day=1,hour = 0, minute = 0, second = 0)
    last_day = yesterday.replace(month= month, day=day_count,hour = 23, minute = 59, second = 59)
    # print("starting date : ",first_day,"\nending date : ",last_day)
    return first_day, last_day


def get_last_15_dates(input_date):
    from datetime import datetime, timedelta
    datetime_object = datetime.strptime(input_date, '%d/%m/%y')
    list_of_last_15_days_date = []
    for i in range(0,15):
        l = (datetime_object - timedelta(days=int(i))).strftime('%d/%m/%y')
        list_of_last_15_days_date.append(str(l))
    return list_of_last_15_days_date

